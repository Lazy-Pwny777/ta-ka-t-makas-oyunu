#!/bin/bash

while :; do

###########################
#a text game by Lazy-pwny.#
###########################
#lisans a gerek yok istediğiniz gibi çarpın ama soran olursa GNU 3.
#the best işsizler oyunu, işsizlik edipoynadığınız için teşekkürler.
#
#DESCRIPTION: Agalar oyun cok basit lakin illa bug yada hata olabilir
#yani bildiğimiz taş kağıt makas oyunu. Bon chanse et bon joue.

t="tas" #1
k="kagit" #2
m="makas" #3

#renkler
kirmizi="\e[91m"
yesil="\e[92m"
mavi="\e[34m"
turuncu="\e[93m"
tp="\e[0m"


#hadi baslayalim.
x=$(( $RANDOM % 3 ))

echo "1) tas" ; echo "2) kagit" ; echo "3) makas"
echo "-----------------------------"
read -p "Give Your Option:" option
echo "-----------------------------"

#egerki kullanici tas derse.
if [[ "$option" =~ ^($t|1|TAS)$ ]] ; then
	if [[ $x = 1 ]] ; then
		echo "[Bot]tas diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]tas, tastan ustun degildir. ${mavi}Beraberlik!$tp"
	elif [[ $x = 2 ]] ; then
		echo "[Bot]kagit diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]kagit, tasi sarar. ${kirmizi}*Bot kazandi*$tp"
	else 
		echo "[Bot]makas diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]tas, makasi kirar. ${yesil}*$HOSTNAME kazandi*$tp"
	fi
#egerki kullanici kagit derse.
elif [[ "$option" =~ ^($k|2|KAGIT)$ ]] ; then
	if [[ $x = 1 ]] ; then
		echo "[Bot]tas diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]kagit, tasi sarar. ${yesil}*$HOSTNAME kazandi*$tp"
	elif [[ $x = 2 ]] ; then
		echo "[Bot]kagit diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]kagit, kagittan ustun degildir. ${mavi}Beraberlik!$tp"
	else
		echo "[Bot]makas diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]makas, kagiti keser. ${kirmizi}*Bot kazandi*$tp"		
	fi
#egerki kullanici makas derse.
elif [[	"$option" =~ ^($m|3|MAKAS)$ ]] ; then
	if [[ $x = 1 ]] ; then
		echo "[Bot]tas diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]tas, makası kirar. ${kirmizi}*Bot kazandi*$tp"
	elif [[ $x = 2 ]] ; then
		echo "[Bot]kagit diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]makas, kagiti keser. ${yesil}*$HOSTNAME kazandi*$tp"
	else
		echo "[Bot]makas diyorum!" ; echo "-----------------------------" ; echo -e "[Hakem]makas, makastan ustun degildir. ${mavi}Beraberlik$tp"
	fi

else
	echo -e "${turuncu}[i]${tp}Bunun bir cevap olduguna eminmisin aga??"
fi

#devam etmek istermisin?
echo ""
read -p "[i]devam etmek istermisin?[e/H]" ifcontinue
case "$ifcontinue" in
	[eE])
		clear
	;;
	*)
		echo "Bye!"
		exit 0
esac
done
